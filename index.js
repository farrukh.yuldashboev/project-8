
const timer = document.querySelector(".timer");
const startBtn = document.querySelector(".btn-start-pause");
const stopBtn = document.querySelector(".btn-add");
const resetBtn = document.querySelector(".btn-reset");
const tasks = document.querySelector(".tasks")

let isActive = false;
let isFirstRender = true;
let interval;

let m = localStorage.getItem("minutes") || 0;
let s = localStorage.getItem("seconds") || 0;

const start = () => {
  interval = setInterval(() => AddTime(), 1000);
};

const AddTime = () => {
  if (s < 59) {
    s++;
  } else {
    m++;
    s = 0;
  }
  refreshTime();
};

const refreshTime = () => {
  const result = `${m < 10 ? "0" + m : m}:${s < 10 ? "0" + s : s}`;
  localStorage.setItem("minutes", m);
  localStorage.setItem("seconds", s);
  timer.innerText = result;
};
const deleteTask = (e) => {
  console.log(e);
};
startBtn.addEventListener("click", (e) => {
  if (!isActive) {
    start();
    isActive = true;
    startBtn.innerHTML = "Pause";
  } else {
    clearInterval(interval);
    isActive = false;
    startBtn.innerHTML = "Start";
    }
});
resetBtn.addEventListener("click", (e) => {
  m = 0;
  s = 0;
  clearInterval(interval);
  localStorage.clear();
  isActive = false;
  refreshTime();
  startBtn.innerHTML = "Start";
  tasks.innerHTML = ""
});
stopBtn.addEventListener("click", (e) => {
  const task = document.createElement("div");
  task.classList.add("task");
  const taskTime = document.createElement("div");
  taskTime.classList.add("task-time");
  taskTime.innerHTML = `${m < 10 ? "0" + m : m}:${s < 10 ? "0" + s : s}`;
  const taskDelete = document.createElement("div");
  taskDelete.classList.add("task-delete");
  taskDelete.innerHTML = "x";
  task.append(taskTime, taskDelete);
  tasks.prepend(task);

});

document.addEventListener("click", (e) => {
  if (e.target.classList.contains("task-delete")) {
    e.target.parentElement.remove()
  }
});

refreshTime();
